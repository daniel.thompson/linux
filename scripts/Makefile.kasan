# SPDX-License-Identifier: GPL-2.0

ifdef CONFIG_CC_HAS_KASAN_MEMINTRINSIC_PREFIX
# Safe for compiler to generate meminstrinsic calls in uninstrumented files.
CFLAGS_KASAN_NOSANITIZE :=
else
# Don't let compiler generate memintrinsic calls in uninstrumented files
# because they are instrumented.
CFLAGS_KASAN_NOSANITIZE := -fno-builtin
endif

KASAN_SHADOW_OFFSET ?= $(CONFIG_KASAN_SHADOW_OFFSET)

cc-param = $(call cc-option, -mllvm -$(1), $(call cc-option, --param $(1)))

ifdef CONFIG_KASAN_STACK
	stack_enable := 1
else
	stack_enable := 0
endif

ifdef CONFIG_KASAN_GENERIC

ifdef CONFIG_KASAN_INLINE
	# When the number of memory accesses in a function is less than this
	# call threshold number, the compiler will use inline instrumentation.
	# 10000 is chosen offhand as a sufficiently large number to make all
	# kernel functions to be instrumented inline.
	call_threshold := 10000
else
	call_threshold := 0
endif

# First, enable -fsanitize=kernel-address together with providing the shadow
# mapping offset, as for GCC, -fasan-shadow-offset fails without -fsanitize
# (GCC accepts the shadow mapping offset via -fasan-shadow-offset instead of
# a --param like the other KASAN parameters).
# Instead of ifdef-checking the compiler, rely on cc-option.
CFLAGS_KASAN := $(call cc-option, -fsanitize=kernel-address \
		-fasan-shadow-offset=$(KASAN_SHADOW_OFFSET), \
		$(call cc-option, -fsanitize=kernel-address \
		-mllvm -asan-mapping-offset=$(KASAN_SHADOW_OFFSET)))

# Now, add other parameters enabled similarly in both GCC and Clang.
# As some of them are not supported by older compilers, use cc-param.
CFLAGS_KASAN += $(call cc-param,asan-instrumentation-with-call-threshold=$(call_threshold)) \
		$(call cc-param,asan-stack=$(stack_enable)) \
		$(call cc-param,asan-instrument-allocas=1) \
		$(call cc-param,asan-globals=1)

# Instrument memcpy/memset/memmove calls by using instrumented __asan_mem*()
# instead. With compilers that don't support this option, compiler-inserted
# memintrinsics won't be checked by KASAN on GENERIC_ENTRY architectures.
CFLAGS_KASAN += $(call cc-param,asan-kernel-mem-intrinsic-prefix=1)

endif # CONFIG_KASAN_GENERIC

ifdef CONFIG_KASAN_SW_TAGS

ifdef CONFIG_KASAN_INLINE
	instrumentation_flags := $(call cc-param,hwasan-mapping-offset=$(KASAN_SHADOW_OFFSET))
else
	instrumentation_flags := $(call cc-param,hwasan-instrument-with-calls=1)
endif

CFLAGS_KASAN := -fsanitize=kernel-hwaddress \
		$(call cc-param,hwasan-instrument-stack=$(stack_enable)) \
		$(call cc-param,hwasan-use-short-granules=0) \
		$(call cc-param,hwasan-inline-all-checks=0) \
		$(instrumentation_flags)

# Instrument memcpy/memset/memmove calls by using instrumented __hwasan_mem*().
ifeq ($(call clang-min-version, 150000)$(call gcc-min-version, 130000),y)
	CFLAGS_KASAN += $(call cc-param,hwasan-kernel-mem-intrinsic-prefix=1)
endif

endif # CONFIG_KASAN_SW_TAGS

export CFLAGS_KASAN CFLAGS_KASAN_NOSANITIZE
